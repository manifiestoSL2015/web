from django import forms

from sign.models import Sign


class SignForm(forms.ModelForm):
    class Meta:
        model = Sign
        widgets = {
                'name': forms.TextInput(attrs={'placeholder': 'Nombre'}),
                'surname': forms.TextInput(attrs={'placeholder': 'Apellidos'}),
                'email': forms.TextInput(attrs={'placeholder': 'Correo electrónico'}),
                'party': forms.TextInput(attrs={'placeholder': 'Partido'}),
                'position': forms.TextInput(attrs={'placeholder': 'Posición'})
        }
        exclude = ['date']
