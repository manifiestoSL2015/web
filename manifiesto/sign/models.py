from django.db import models
from django.utils.translation import ugettext_lazy as _


class Sign(models.Model):
    name = models.CharField(_("Nombre"), max_length=200)
    surname = models.CharField(_("Apellidos"), max_length=200)
    email = models.EmailField(_("Correo Electrónico"))
    party = models.CharField(_("Partido"), max_length=200, blank=True, null=True)
    position = models.CharField(_("Cargo"), max_length=200, blank=True, null=True)

    date = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.name
