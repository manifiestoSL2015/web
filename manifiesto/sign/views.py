from django.forms import ModelForm
from django.shortcuts import get_object_or_404, render, redirect

from sign.forms import SignForm
from sign.models import Sign


def sign_new(request):
    if request.method == "POST":
        form = SignForm(request.POST)
        if form.is_valid():
            sign = form.save(commit=False)
            sign.save()
            return redirect('sign_show')
    else:
        form = SignForm()
    return render(request, 'sign_new.html', {'form': form})


def sign_show(request):
    signs = Sign.objects.all()
    return render(request, 'sign_show.html', {'signs': signs})
