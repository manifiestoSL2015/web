from django.contrib import admin

from .models import Sign


class SignAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'email', 'party', 'position', 'date')
    list_filter = ('party',)
    search_fields = ('name', 'surname', 'party')

    date_hierarchy = 'date'


admin.site.register(Sign, SignAdmin)
