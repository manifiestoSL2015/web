# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sign',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('surname', models.CharField(verbose_name='Surname', max_length=200)),
                ('email', models.EmailField(verbose_name='Email', max_length=254)),
                ('party', models.CharField(null=True, verbose_name='Party', blank=True, max_length=200)),
                ('position', models.CharField(null=True, verbose_name='Position', blank=True, max_length=200)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
