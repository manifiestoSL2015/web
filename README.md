# Install

You need virtualenv in your OS before exec the following commands:

Create virtualenv and install requirements:

```
virtualenv . -p python3
source bin/activate
pip install -r requirements.txt
```

Create database, apply fixture and run project:

```
cd manifiesto
./manage.py migrate
./manage.py dumpdata initial-data.json
./manage.py runserver
```
